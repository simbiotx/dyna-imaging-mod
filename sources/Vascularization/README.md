# Vascularization

## Requirements
This module works only with the Nix library. See [the dedicated repository](https://gitlab.inria.fr/simbiotx/dyna-imaging-mod/-/tree/main/sources/libnix) for further information.

## Launch parameters
All launch parameters and what they do in the `Perfusion.cpp` file.
* `F` : Computes the distance to a root node.
* `a` : Set an initial intersitial concentration. Default = 0
* `b` : Set an initial plasma concentration. Default = 0
* `f` : Set an initial value for the flow. Default = 0
* `W` : print file to POVray
* `B` : Set test type to Irregular Lattice (see `-c` for further use)
* `A` : Test mode
* `d` : Set the diffusion parameter to the given one. Default = 0
* `t` : Set the time step (in seconds) for the transport. Default = optimized dt
* `e` : Set the maximum time for the transport. Default = 180s
* `o` : Set the delay between two output for the transport. Default = 1s
* `X` : Write the output to  (mandatory to reuse vascularization)
* `p` : Set the default permeability of the vessels. Default = 1
* `P` : Set the default microvascular density. Default = 1
* `x, y, z` : Set the size of the domain. If z = 1: dimension < 3, if y = 1, dimension = 1  
* `r` : Set the radius of the necrotic zone. Default = 0
* `V` : Create the vascularization object, from the x,y,z dimensions or recover from a file.
* `9` : Compute and print KPS, flow, phip ; temporary
* `D` : Write commands in a document "commandline.dat"
* `T` : Create a tumor
    * `x,y,z` : Set position of the tumor
    * `r` : Set the radius of the tumor
    * `p` : Set the specific permeability inside the tumor (outdated)
    * `c` : Set the microvascular density of the tumor  

    Any of the previous command (`-T.`) creates the tumor if there isn't already one. There cannot be more than one tumor.
* `N` : Creates a necrotic core to the tumor with a radius set by `-r`
* `I` : Initializes the vascularisation
    * `symmetric`: Creates two binary trees, one arterial facing one venous tree at z = MAX_Z/2
    * `singleStraight`: Creates a straight vessel of length MAX_X, in the middle of the domain. The two extreme points of the vessel are the roots. The vessel radius is set to be fixed as the maximum radius
    * `singleRandom`: Creates a crossing vessel from x=0 to x=MAX_X. The first node is the arterial root, the last node is the venous root. At each node, the next node is chosen randomly between forward, above or below uniformly. The radius of the vessel is set to be constant as the maximum radius.
    * any other: 
        * Arterial root:
            * Dim 1: (0,0,0). 
            * Dim 2:
                * Type 0,1: Center
                * Type 2: 1/2, 2/3
                * Type 3: 3/4, 1/2
            * Dim 3: (1/2,1/2,0)
        * Other roots:
        Add roots iteratively at each border of the domain until there are enough roots (the distance between roots should be larger than 50)
            * Type 1: Alternate between arteries and veins (depending on which is closer)
            * Type 0,2: Venous roots
            * Type 3: add arterial roots at (1/2,3/4,0) and (1/3,1/5,0)
        Adds one vessel to all the roots, normal to the border then add branches to all the vessels. Sprout the resulting vessels once.
* `S` : Same as `-Isymmetric`
* `1` : Same as `-IsingleStraight`
* `2` : Same as `-IsingleRandom`
* `R` : Remodel the vascular network by homogeneisation of the shear stress
* `O` : Output everything to eps (pov if dim>3)
* `C` : Perform the perfusion by a contrast agent. The argument defines the used scheme.
    * `upwind`
    * `lax`
    * `minmod`
    * `superbee`
    * `euler`

    Default: implicit
    The scheme can be refined by adding `,refineX` ; `X` being the chosen refinement. Default = 0
